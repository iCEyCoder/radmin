<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function(){
    Route::get('/login',['uses' => 'AuthController@getAdminLogin'])->name('getAdminLogin');
    Route::post('/login',['uses' => 'AuthController@postAdminLogin'])->name('postAdminLogin');
    Route::get('/logout',['uses' => 'AuthController@getAdminLogout'])->name('getAdminLogout');
    Route::middleware(['auth'])->group(function(){
        Route::get('/',['uses' => 'Admin\HomeController@getHome'])->name('getAdminHome');
        Route::get('user/create',['uses' => 'Admin\UserController@getUserCreate'])->name('getAdminCreateUser');
        Route::post('user/create',['uses' => 'Admin\UserController@postUserCreate'])->name('postAdminCreateUser');
        Route::get('user/authlog',['uses' => 'Admin\LogController@getAdminRadAuthLog'])->name('getAdminRadAuthLog');
        Route::get('user/{id}/del',['uses' => 'Admin\UserController@getUserDelete'])->name('getAdminDeleteUser');
        Route::get('user/{id}',['uses' => 'Admin\UserController@getUserUpdate'])->name('getAdminUpdateUser');
        Route::post('user/{id}',['uses' => 'Admin\UserController@postUserUpdate'])->name('postAdminUpdateUser');
    });
});

Route::middleware(['auth:radcheck'])->group(function(){
    Route::get('/',['uses' => 'HomeController@getRadHome'])->name('getRadHome');
    Route::get('/home',['uses' => 'HomeController@getRadHome'])->name('getRadHome');
});

Route::middleware(['guest'])->group(function(){
    Route::get('/login',['uses' => 'AuthController@getRadLogin'])->name('getRadLogin');
    Route::post('/login',['uses' => 'AuthController@postRadLogin'])->name('postRadLogin');
    Route::get('/help',['uses' => 'HomeController@getHelp'])->name('getHelp');
});

Route::get('/logout',['uses' => 'AuthController@getRadLogout'])->name('getRadLogout');
// Route::any('.*',['uses' => 'AuthController@getLogin']);
