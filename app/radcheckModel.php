<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class radcheckModel extends Authenticatable
{
    use Notifiable;
    protected $table = "radcheck";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'value', 'op'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'op'
    ];

    public static function Attempt($credentials){
        $result = radcheckModel::where('username','=',$credentials['username'])
        ->where('value','=',$credentials['password'])
        ->get();
        if($result->isEmpty()){
            return false;
        }
        return $result->first();
    }

    public static function CreateUser($username,$password,$admin_id){
        
        $data = new radcheckModel();
        $data->username = $username;
        $data->attribute = "Cleartext-password";
        $data->op = ":=";
        $data->value = $password;
        $data->save();

        $mn = new radwebModel();
        $mn->admin_id = $admin_id;
        $mn->user_id = $data->id;
        $mn->save();
        
    }

    public static function FindUser($id,$admin_id){
        $user = radcheckModel::where('radcheck.id','=',$id);
        $user = $user->join('rad_to_web','radcheck.id','=','rad_to_web.user_id')
        ->where('rad_to_web.admin_id','=',$admin_id);
        return $user;
    }

    public static function UpdateUser($id,$username,$password,$admin_id){
        $user = radcheckModel::where('radcheck.id','=',$id);
        $user = $user->join('rad_to_web','radcheck.id','=','rad_to_web.user_id')
        ->where('rad_to_web.admin_id','=',$admin_id);
        $user->update(['username' => $username,'value' => $password]);
        return $user;
    }

    public static function DeleteUser($id,$admin_id){
        try{
            DB::transaction(function() use ($id,$admin_id){
                radcheckModel::where('radcheck.id','=',$id)->delete();
                radwebModel::where('user_id','=',$id)->where('admin_id','=',$admin_id)->delete();
            });
            return true;
        }catch(\Exception $e){
            return false;
        }
    }
    
}
