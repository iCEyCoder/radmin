<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        // return admins to admin area
        if (! $request->expectsJson() && preg_match("@/admin(.*?)@",$request->getRequestUri())) {
            return route('getAdminLogin');
        }
        // return anyother user to regular login
        if(! $request->expectsJson()) {
            return route('getRadLogin');
        }
    }
}
