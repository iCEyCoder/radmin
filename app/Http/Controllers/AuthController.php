<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\radcheckModel;

class AuthController extends Controller
{
    public function getLogin(){
        return View('radmin.login');
    }

    public function postLogin(){
        $result = Auth::attempt(['name' => 'n','password' => '123']);
        if($result){
            return redirect()->route('getRadHome');
        }else{
            return redirect()->route('getRadLogin');
        }
    }

    public function getSignup(){

    }

    public function getRecovery(){
        
    }

    public function getRadLogout(){
        // logout freeradius users
        Auth::guard('radcheck')->logout();

        return redirect()->route('getRadLogin');
    }

    public function getRadLogin(){
        return View('radmin.login');
    }
    
    public function postRadLogin(Request $request){
        $request->validate([
            'username' => 'required|min:3',
            'password' => 'required|min:3'
        ]);

        $user = $request->input('username');
        $pass = $request->input('password');
        // since laravel needs hash passwords and freeradius operates in variety of ways
        // we need to make a dedicated function to support all of it.
        $result = radcheckModel::Attempt(['username' => $user , 'password' => $pass]);

        if($result){
            Auth::guard('radcheck')->loginUsingId($result->id);
            return redirect()->route('getRadHome');
        }else{
            Log::warning("FailedUserLoign : " . $user . " " . $pass . " " . \Request::ip());
            return redirect()->route('getRadLogin')->with(['error' => 'Incorrect username or password']);
        }

    }

    public function getAdminLogout() {
        // logout admins
        Auth::logout();
        return redirect()->route('getAdminLogin');
    }


    public function getAdminLogin(){
        return View('radmin.login');
    }

    public function postAdminLogin(Request $request){
        $request->validate([
            'username' => 'required|min:3|alpha_num',
            'password' => 'required|min:6'
        ]);

        $user = $request->input('username');
        $pass = $request->input('password');

        if(Auth::attempt(['name' => $user, 'password' => $pass])){
            return redirect()->route('getAdminHome');
        }

        Log::warning("FailedAdminLoign : " . $user . " " . $pass . " " . \Request::ip());
        return redirect()->route('getAdminLogin')->with(['error' => 'Incorrect username or password']);
        
    }

}
