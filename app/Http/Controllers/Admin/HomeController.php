<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\radcheckModel;

class HomeController extends Controller
{
    public function getHome(){
        $nm = radcheckModel::join('rad_to_web','radcheck.id','=','rad_to_web.user_id')
            ->where('admin_id','=',Auth::User()->id)
            ->get(['radcheck.id as id','radcheck.username as username']);
            
        return view('radmin.admin.home')->with(['users' => $nm]);
    }
}
