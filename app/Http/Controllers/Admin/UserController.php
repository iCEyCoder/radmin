<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\radcheckModel;

class UserController extends Controller
{
    
    public function getUserCreate () {
        return view('radmin.admin.user');
    }

    public function postUserCreate(Request $request) {
        $request->validate([
            'username' => 'required|min:3|unique:radcheck,username|alpha_num',
            'password' => 'required|min:6'
        ]);
        $username = strtolower(Auth::User()->name) . "-" . $request->input('username');
        $password = $request->input('password');

        $result['ok'] = radcheckModel::CreateUser($username, $password, Auth::User()->id);

        return view('radmin.admin.user')->with($result);
    }

    public function getUserUpdate($id){
        $data = radcheckModel::FindUser($id,Auth::User()->id);
        $data = $data->get(['radcheck.id as id','radcheck.username as username','radcheck.value as password']);
        
        if($data->isEmpty()){
            return redirect()->route('getAdminHome');
        }
        $data = $data->first();
        $admin_name = Auth::User()->name;
        $data->username = \preg_replace("@^$admin_name-@i","",$data->username);

        return view('radmin.admin.user')->with(['user' => $data]);
    }

    public function postUserUpdate(Request $request,$id){
        $request->validate([
            'username' => 'required|min:3|alpha_num',
            'password' => 'required|min:6'
        ]);
        $username = $request->input('username');
        $password = $request->input('password');
        $admin_id = Auth::User()->id;
        $admin_name = Auth::User()->name;
        // check if this admin has privilege to change users attributes
        $data = radcheckModel::FindUser($id,$admin_id);

        if(!$data->count()){
            return redirect()->route('getAdminHome');
        }
        if(\preg_match("@^$admin_name-@i",$data->first()->username)){
            $username = strtolower(Auth::User()->name) . "-" . $username;
        }

        radcheckModel::UpdateUser($id, $username, $password, Auth::User()->id);
        return redirect()->route('getAdminUpdateUser',[$id]);
    }

    public function getUserDelete(Request $request,$id){
        
        $admin_id = Auth::user()->id;
        $data = radcheckModel::FindUser($id,$admin_id);
        $username = $data->first()->username;

        if($data->count()){
            if(radcheckModel::DeleteUser($id, $admin_id)){
                return redirect()->route('getAdminHome')->with('message','user succesfully deleted.');
            }else{
                return redirect()->route('getAdminHome')->with('message','an error occourd');
            }
        }
    }

}
