<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\radcheckModel;


class LogController extends Controller
{
    public function getAdminRadAuthLog () {

        $adminID = Auth::User()->id;

        $n = radcheckModel::join('radpostauth','radcheck.username','=','radpostauth.username')
        ->join('rad_to_web','rad_to_web.user_id','=','radcheck.id')
        ->where('rad_to_web.admin_id','=',$adminID)
        ->orderByDesc('radpostauth.authdate')
        ->get(['radpostauth.username','radpostauth.pass','radpostauth.reply','radpostauth.authdate','radpostauth.callingstationid','radpostauth.nasipaddress']);

        return view('radmin.admin.log')->with(['v_page_name'=> 'Authentication logs','logdata'=>$n]);

    }
}