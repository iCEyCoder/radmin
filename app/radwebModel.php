<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class radwebModel extends Model
{
    protected $table = "rad_to_web";
    public $timestamps = false;
}
